import java.io.File
import java.io.InputStream

abstract class BaseDay<T> {
    protected abstract val puzzleFileName: String

    abstract fun solvePuzzle(): T

    protected fun readFile(): List<String> {
        return File("puzzles/$puzzleFileName").readLines()
    }
}
