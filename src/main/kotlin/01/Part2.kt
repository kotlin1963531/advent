package `01`

import BaseDay

class Part2() : BaseDay<Int>() {
    override val puzzleFileName: String = "day1.txt";

    private var result: Int = 0;

    private val digitMap = mapOf("one" to "1", "two" to "2", "three" to "3", "four" to "4", "five" to "5", "six" to "6", "seven" to "7", "eight" to "8", "nine" to "9")
    private val digitRegex = "\\d|one|two|three|four|five|six|seven|eight|nine".toRegex()


    override fun solvePuzzle():  Int {
        val fileDataLines = readFile();

        for (line in fileDataLines) {
            val digits = digitRegex.findAll(line).map { match ->
                digitMap[match.value] ?: match.value
            }.toList()

//            println("${digits}")
//            println("${line} - ${digits.first()}:${digits.last()}")
//            if (digits.isEmpty() || digits.size < 2) {
//                continue;
//            }

            val firstDigit = digits.first()
            val lastDigit = digits.last()

            result += "${firstDigit}${lastDigit}".toInt();
        }

        return result
    }
}
