package `01`

import BaseDay

class Part1() : BaseDay<Int>() {
    override val puzzleFileName: String = "day1.txt";

    private var result: Int = 0;

    override fun solvePuzzle():  Int {
        val fileDataLines = readFile();

        for (line in fileDataLines) {
            val firstDigit = line.firstOrNull { it.isDigit() }
            val lastDigit = line.lastOrNull { it.isDigit() }

            if (firstDigit == null || lastDigit == null) {
                continue;
            }

            result += "${firstDigit}${lastDigit}".toInt();
        }
        return result
    }
}
