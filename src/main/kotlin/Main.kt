import `01`.Part1
import `01`.Part2

fun main(args: Array<String>) {
    println("Day 1.1 -> ${(Part1()).solvePuzzle()}")
    println("Day 1.2 -> ${(Part2()).solvePuzzle()}")
}
